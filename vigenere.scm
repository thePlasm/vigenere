(define
	(correct-key key)
	(if
		(= (string-length key) 0)
		""
		(if 
			(and
				(>= (char->integer (string-ref key 0)) (char->integer #\A))
				(<= (char->integer (string-ref key 0)) (char->integer #\Z)))
			(string-append (string (string-ref key 0)) (correct-key (substring key 1 (string-length key))))
			(correct-key (substring key 1 (string-length key))))))

(define
	(correct-length str num)
	(let
		((l (string-length str)))
		(cond
		((< num l) 
			(substring str 0 num))
		((= num l)
			str)
		(else
			(string-append str (correct-length str (- num l)))))))

(define
	(enc-char plain key)
	(if
		(and
			(>= (char->integer plain) (char->integer #\A))
			(<= (char->integer plain) (char->integer #\Z)))
		(integer->char
			(+
				(char->integer #\A)
				(remainder
					(+
						(-
							(char->integer plain)
							(char->integer #\A))
						(-
							(char->integer key)
							(char->integer #\A)))
					26)))
		plain))

(define
	(vigenere plain key)
	(if
		(and (string-upper-case? plain) (string-upper-case? key) (= (string-length plain) (string-length key)))
		(cond
			((= 0 (string-length plain))
				"")
			((= 1 (string-length plain))
				(string
					(enc-char
						(string-ref plain 0)
						(string-ref key 0))))
			(else 
				(string-append
					(string
						(enc-char
							(string-ref plain 0)
							(string-ref key 0)))
					(vigenere (substring plain 1 (string-length plain)) (substring key 1 (string-length key))))))
		(vigenere (string-upcase plain) (correct-length (correct-key (string-upcase key)) (string-length plain)))))